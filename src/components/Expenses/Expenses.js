import React, { useState } from 'react'
import ExpenseFilter from "./ExpenseFilter"
import ExpenseItem from "./ExpenseItem";
import Card from "../UI/Card"
import './Expenses.css'

const Expenses = (props) => {
  const [filterYear, setFilterYear] = useState('2020');

  const selectedYearHandler = (year) => {
    setFilterYear(year);
  };

  // let expenses = props.updateExpenses(props.items.map((expense) => <ExpenseItem item={expense}/>))

  return (
    <Card className='expenses'>
      <ExpenseFilter selected={filterYear} selectedYear={selectedYearHandler}/>
      {/* {expenses} */}
    </Card>

  );
}

export default Expenses